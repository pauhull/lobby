package de.pauhull.hidestick.util;

import de.pauhull.hidestick.HideStick;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

public class StickUtil {

	public static void updatePlayerVisibility(Player player, HideStick.VisibilityMode mode) {
		for(Player all : Bukkit.getOnlinePlayers()) {
			if(player.equals(all)) continue;
			switch(mode) {
			case VISIBLE:
				player.showPlayer(all);
				break;
			case HIDDEN:
				player.hidePlayer(all);
				break;
			case VIP:
				if(all.hasPermission(HideStick.getInstance().getVipPermission())) {
					player.showPlayer(all);
				} else {
					player.hidePlayer(all);
				}
				break;
			}
		}
	}
	
}
