package de.pauhull.hidestick.util;

import de.pauhull.hidestick.HideStick;
import org.bukkit.Bukkit;
import org.bukkit.inventory.Inventory;

public class StickInventory {
	
	public static Inventory getHideInventory(String title) {
		
		Inventory inventory = Bukkit.createInventory(null, 9, title);
		
		inventory.setItem(2, HideStick.getInstance().getVisibleStack());
		inventory.setItem(4, HideStick.getInstance().getVipStack());
		inventory.setItem(6, HideStick.getInstance().getHiddenStack());
		
		return inventory;
	}
	
}
