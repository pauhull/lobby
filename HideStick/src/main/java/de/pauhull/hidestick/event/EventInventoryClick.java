package de.pauhull.hidestick.event;

import de.pauhull.hidestick.HideStick;
import de.pauhull.hidestick.util.StickUtil;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public class EventInventoryClick implements Listener {

	private HideStick hideStick;
	
	public EventInventoryClick(HideStick hideStick) {
		this.hideStick = hideStick;
	
		Bukkit.getPluginManager().registerEvents(this, hideStick.getLobby().getPlugin());
	}
	
	@EventHandler
	public void onInventoryClick(InventoryClickEvent event) {
		Player player = (Player) event.getWhoClicked();
		Inventory inventory = event.getInventory();

		if(inventory == null) return;
		
		if(!inventory.equals(hideStick.getStickInventory())) {
			return;
		}

		if(event.getCurrentItem() == null) return;
		if(event.getCurrentItem().getType() == Material.AIR) return;

        ItemStack stack = event.getCurrentItem();
		event.setCancelled(true);
		player.playSound(player.getLocation(), hideStick.getMenuSound(), 1f, 1f);

        HideStick.VisibilityMode mode = null;

        if(stack.equals(hideStick.getVisibleStack())) mode = HideStick.VisibilityMode.VISIBLE;
        if(stack.equals(hideStick.getHiddenStack())) mode = HideStick.VisibilityMode.HIDDEN;
        if(stack.equals(hideStick.getVipStack())) mode = HideStick.VisibilityMode.VIP;

		hideStick.getVisibilityMode().put(player.getUniqueId(), mode);
		player.getInventory().setItem(hideStick.getHotbarSlot(), stack);
		StickUtil.updatePlayerVisibility(player, mode);

		player.updateInventory();
		player.closeInventory();
	}
	
}
