package de.pauhull.hidestick.event;

import de.pauhull.hidestick.HideStick;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

public class EventPlayerQuit implements Listener {

	HideStick hideStick;
	
	public EventPlayerQuit(HideStick hideStick) {
		this.hideStick = hideStick;
		
		Bukkit.getPluginManager().registerEvents(this, hideStick.getLobby().getPlugin());
	}
	
	@EventHandler
	public void onQuit(PlayerQuitEvent event) {
		Player player = event.getPlayer();
		
		if(hideStick.getVisibilityMode().containsKey(player.getUniqueId())) {
			hideStick.getVisibilityMode().remove(player.getUniqueId());
		}
	}
	
}
