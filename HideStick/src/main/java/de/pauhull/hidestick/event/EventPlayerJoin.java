package de.pauhull.hidestick.event;

import de.pauhull.hidestick.HideStick;
import de.pauhull.hidestick.util.StickUtil;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

public class EventPlayerJoin implements Listener {

	private HideStick hideStick;
	
	public EventPlayerJoin(HideStick hideStick) {
		this.hideStick = hideStick;
		
		Bukkit.getPluginManager().registerEvents(this, hideStick.getLobby().getPlugin());
	}

	@EventHandler
	public void onPlayerJoin(PlayerJoinEvent event) {
		
		for(Player player : Bukkit.getOnlinePlayers()) {
			StickUtil.updatePlayerVisibility(player, hideStick.getVisibilityMode().getOrDefault(player.getUniqueId(),
					HideStick.VisibilityMode.VISIBLE));
		}
		
		Player player = event.getPlayer();

		if(hideStick.getHotbarSlot() != -1) {
			player.getInventory().setItem(hideStick.getHotbarSlot(), hideStick.getVisibleStack());
		}
		
	}
	
}
