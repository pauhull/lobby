package de.pauhull.hidestick.event;

import de.pauhull.hidestick.HideStick;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

public class EventPlayerInteract implements Listener {

	private HideStick hideStick;
	
	public EventPlayerInteract(HideStick hideStick) {
		this.hideStick = hideStick;
		
		Bukkit.getPluginManager().registerEvents(this, hideStick.getLobby().getPlugin());
	}
	
	@EventHandler
	public void onPlayerInteract(PlayerInteractEvent event) {
		
		Player player = event.getPlayer();
		
		if(event.getAction() != Action.RIGHT_CLICK_AIR && event.getAction() != Action.RIGHT_CLICK_BLOCK) {
			return;
		}
		
		if(player.getItemInHand() == null) {
			return;
		}
		
		ItemStack stack = player.getItemInHand();

		if(stack.equals(hideStick.getVisibleStack()) || stack.equals(hideStick.getVipStack()) || stack.equals(hideStick.getHiddenStack())) {
			player.playSound(player.getLocation(), hideStick.getMenuSound(), 1f, 1f);
			player.openInventory(hideStick.getStickInventory());
		}
		
	}

}
