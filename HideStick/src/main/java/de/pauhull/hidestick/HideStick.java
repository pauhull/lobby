package de.pauhull.hidestick;

import de.pauhull.hidestick.event.EventInventoryClick;
import de.pauhull.hidestick.event.EventPlayerInteract;
import de.pauhull.hidestick.event.EventPlayerJoin;
import de.pauhull.hidestick.event.EventPlayerQuit;
import de.pauhull.hidestick.util.StickInventory;
import de.pauhull.lobby.Lobby;
import de.pauhull.lobby.module.FileManager;
import de.pauhull.lobby.module.Module;
import de.pauhull.lobby.util.Configuration;
import de.pauhull.lobby.util.ItemUtil;
import lombok.Getter;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class HideStick extends Module {

	private static Logger logger = LoggerFactory.getLogger(HideStick.class);

	@Getter
	private Configuration config;

	@Getter
	private Map<UUID, VisibilityMode> visibilityMode = new HashMap<>();

	@Getter
	private ItemStack visibleStack, hiddenStack, vipStack;

	@Getter
	private Inventory stickInventory;

	@Getter
	private int hotbarSlot;

	@Getter
	private Sound menuSound;

	@Getter
	private String vipPermission;

	@Getter
	private static HideStick instance;

	public HideStick(Lobby lobby) {
		super(lobby);
	}

	@Override
	public void enable() {

		instance = this;

		loadConfig();

		// Load events
		new EventPlayerInteract(this);
		new EventPlayerJoin(this);
		new EventInventoryClick(this);
		new EventPlayerQuit(this);

	}

	private void loadConfig() {

		File from = FileManager.getCachedFile("config.yml", this);
		File to = new File("plugins/Lobby/modules/HideStick/config.yml");

		to.getParentFile().mkdirs();

		if(!to.exists()) {

			try {
				Files.copy(from.toPath(), to.toPath());
			} catch (IOException e) {
				logger.error("Couldn't copy config. No write permissions?");
				e.printStackTrace();
			}

		}

		this.config = new Configuration(to);

		this.visibleStack = ItemUtil.readFromConfig(config.getConfig(), "General.Visible");
		this.hiddenStack = ItemUtil.readFromConfig(config.getConfig(), "General.Hidden");
		this.vipStack = ItemUtil.readFromConfig(config.getConfig(), "General.VIP");
		this.hotbarSlot = config.getConfig().getInt("General.Hotbar");
		this.vipPermission = config.getConfig().getString("Permissions.VIP");

		try {
			this.menuSound = Sound.valueOf(config.getConfig().getString("General.Sound"));
		} catch(Exception exception) {
			logger.error("Couldn't load sound given in config. Using \"NOTE_BASS\".");
			this.menuSound = Sound.NOTE_BASS;
		}

		this.stickInventory = StickInventory.getHideInventory(ChatColor.translateAlternateColorCodes('&',
				config.getConfig().getString("General.Title"))
				.replaceAll("%C", ":")
				.replace('%', ' '));

	}

	public enum VisibilityMode {
		VISIBLE, VIP, HIDDEN
	}

}

