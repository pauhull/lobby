package de.pauhull.lobbyswitcher;

import de.pauhull.lobby.Lobby;
import de.pauhull.lobby.module.Module;
import de.pauhull.lobby.util.Configuration;

import java.io.File;

public class LobbySwitcher extends Module {

    private Configuration config;

    public LobbySwitcher(Lobby lobby) {
        super(lobby);
    }

    @Override
    public void enable() {

    }

    private void loadConfig() {
        File path = new File("plugins/Lobby/modules/LobbySwitcher");
        path.mkdirs();

        this.config = new Configuration(new File(path, "config.yml"));

    }

}
