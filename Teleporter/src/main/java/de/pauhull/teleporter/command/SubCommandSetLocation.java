package de.pauhull.teleporter.command;

import de.pauhull.lobby.annotation.SubCommand;
import de.pauhull.lobby.command.CommandManager;
import de.pauhull.lobby.command.SubCommandExecutor;
import de.pauhull.teleporter.Teleporter;
import de.pauhull.teleporter.waypoint.Waypoint;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class SubCommandSetLocation implements SubCommandExecutor {

	Teleporter teleporter;
	
	public SubCommandSetLocation(Teleporter teleporter) {
		this.teleporter = teleporter;
		
		CommandManager.registerSubCommand(this);
	}

	@SubCommand(name = "setlocation", commandDescription = "Set Location for Teleporter", usage = "/lobby teleporter setlocation <TeleportID>", permission = "lobby.lobby.teleporter.setlocation")
	@Override
	public boolean onSubCommand(CommandSender commandSender, String[] args) {
		

		if(args.length < 2)
			return false;
		
		if(!args[0].equalsIgnoreCase("teleporter") || !args[1].equalsIgnoreCase("setlocation"))
			return false;

		Player player;

		if(commandSender instanceof Player) {
			player = (Player) commandSender;
		} else {
		//	commandSender.sendMessage(teleporter.getLobby().getLocale().getMessage("Error.OnlyPlayers", true).build());
			return true;
		}

		if(args.length < 3) {
		//	player.sendMessage(teleporter.getLobby().getLocale().getMessage("General.Prefix", false).build()
		//			+ "§c/lobby teleporter setlocation <TeleportID>");
			return true;
		}
		
		String teleportID = args[2];
		boolean success = false;

		for(Waypoint waypoint : teleporter.getTeleporterInventory().getWaypoints()) {
			if(waypoint.getId().equalsIgnoreCase(teleportID)) {
				success = true;
				teleporter.getLobby().getLocationManager().saveLocation(teleportID, player.getLocation());
				//player.sendMessage(teleporter.getLobby().getLocale().getMessageFromConfig(teleporter.getConfig().getConfig(),
				//		"Messages.TpSet", true)
				//		.placeholder("TP", teleportID)
				//		.build());
			}
		}

		if(!success) {
			//player.sendMessage(teleporter.getLobby().getLocale().getMessageFromConfig(teleporter.getConfig().getConfig(),
			//		"Messages.TpNotFound", true).build());
		}

		return true;
		
	}

}
