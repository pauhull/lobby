package de.pauhull.teleporter.command;

import de.pauhull.lobby.annotation.SubCommand;
import de.pauhull.lobby.command.CommandManager;
import de.pauhull.lobby.command.SubCommandExecutor;
import de.pauhull.teleporter.Teleporter;
import org.bukkit.command.CommandSender;

public class SubCommandReload implements SubCommandExecutor {

	private Teleporter teleporter;
	
	public SubCommandReload(Teleporter teleporter) {
		this.teleporter = teleporter;
		
		CommandManager.registerSubCommand(this);
	}

	@SubCommand(name = "reload", commandDescription = "Reload the Teleporter Module", usage = "/lobby teleporter reload", permission = "lobby.teleporter.reload")
	@Override
	public boolean onSubCommand(CommandSender commandSender, String[] args) {
		if(!args[0].equalsIgnoreCase("teleporter")) return false;
		
		if(args.length < 2) return false;
		
		if(!args[1].equalsIgnoreCase("reload")) {
			return false;
		}

		teleporter.loadConfig();

		//commandSender.sendMessage(teleporter.getLobby().getLocale()
		//		.getMessageFromConfig(teleporter.getConfig().getConfig(), "Messages.Reload", true).build());
		return true;
		
	}
	
}
