package de.pauhull.teleporter.event;

import de.pauhull.lobby.util.ItemUtil;
import de.pauhull.teleporter.Teleporter;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

public class EventPlayerInteract implements Listener {

	private Teleporter teleporter;
	
	public EventPlayerInteract(Teleporter teleporter) {
		this.teleporter = teleporter;
		
		Bukkit.getPluginManager().registerEvents(this, teleporter.getLobby().getPlugin());
	}
	
	@EventHandler
	public void onPlayerInteract(PlayerInteractEvent event) {
		Player player = event.getPlayer();

		ItemStack inHand = player.getItemInHand();

		if(event.getAction() != Action.RIGHT_CLICK_AIR && event.getAction() != Action.RIGHT_CLICK_BLOCK)
			return;

		if(inHand == null || inHand.getItemMeta() == null || inHand.getItemMeta().getDisplayName() == null)
			return;

		ItemStack stack = ItemUtil.readFromConfig(teleporter.getConfig().getConfig(), "Settings.Teleporter");

		if(inHand.equals(stack)) {
			player.playSound(player.getLocation(), teleporter.getSound(), 1, 1);
			player.openInventory(teleporter.getTeleporterInventory().loadInventory());
		}
		
	}
	
}
