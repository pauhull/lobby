package de.pauhull.teleporter.event;

import de.pauhull.teleporter.Teleporter;
import de.pauhull.teleporter.waypoint.Waypoint;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public class EventPlayerInventoryClick implements Listener {

	private Teleporter teleporter;
	
	public EventPlayerInventoryClick(Teleporter teleporter) {
		this.teleporter = teleporter;
		
		Bukkit.getPluginManager().registerEvents(this, teleporter.getLobby().getPlugin());
	}
	
	@EventHandler
	public void onInventoryClick(InventoryClickEvent event) {
		
		Player player = (Player)event.getWhoClicked();
		Inventory inventory = event.getClickedInventory();
		
		if(inventory == null) return;
		
		if(!inventory.getTitle().equals(ChatColor.translateAlternateColorCodes('&', 
				teleporter.getConfig().getConfig().getString("Settings.Title"))))
			return;

		event.setCancelled(true);
		
		if(event.getCurrentItem() == null) return;
		if(event.getCurrentItem().getItemMeta() == null) return;
		if(event.getCurrentItem().getItemMeta().getDisplayName() == null) return;
		
		ItemStack stack = event.getCurrentItem();

		for(Waypoint waypoint : teleporter.getTeleporterInventory().getWaypoints()) {
			if(waypoint.getStack().equals(stack)) {
				teleporter.getLobby().getLocationManager().getLocation(waypoint.getId(), (location) -> {
					if(location != null) {
						player.teleport(location);
						player.playSound(player.getLocation(), teleporter.getSound(), 1, 1);
					} else {
				//		player.sendMessage(teleporter.getLobby().getLocale()
				//				.getMessageFromConfig(teleporter.getConfig().getConfig(), "Messages.TpNotFound",
				//						true).build());
					}
				});
			}
		}
		
	}
	
}
