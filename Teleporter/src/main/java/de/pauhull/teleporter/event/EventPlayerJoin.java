package de.pauhull.teleporter.event;

import de.pauhull.lobby.util.Configuration;
import de.pauhull.lobby.util.ItemUtil;
import de.pauhull.teleporter.Teleporter;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.inventory.ItemStack;

public class EventPlayerJoin implements Listener {

	private Teleporter teleporter;
	
	public EventPlayerJoin(Teleporter teleporter) {
		this.teleporter = teleporter;
		
		Bukkit.getPluginManager().registerEvents(this, teleporter.getLobby().getPlugin());
	}
	
	@EventHandler
	public void onJoin(PlayerJoinEvent event) {

		Player player = event.getPlayer();
		Configuration config = teleporter.getConfig();

		int slot = config.getConfig().getInt("Settings.Hotbar");
		if(slot < 0) return;

		ItemStack stack = ItemUtil.readFromConfig(config.getConfig(), "Settings.Teleporter");

		player.getInventory().setItem(slot, stack);
		
	}
	
}
