package de.pauhull.teleporter;

import de.pauhull.lobby.Lobby;
import de.pauhull.lobby.module.FileManager;
import de.pauhull.lobby.module.Module;
import de.pauhull.lobby.util.Configuration;
import de.pauhull.teleporter.command.SubCommandReload;
import de.pauhull.teleporter.command.SubCommandSetLocation;
import de.pauhull.teleporter.event.EventPlayerInteract;
import de.pauhull.teleporter.event.EventPlayerInventoryClick;
import de.pauhull.teleporter.event.EventPlayerJoin;
import lombok.Getter;
import org.bukkit.Sound;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

public class Teleporter extends Module {

	private static final Logger logger = LoggerFactory.getLogger(Teleporter.class);

	@Getter
	private Configuration config;

	@Getter
	private TeleporterInventory teleporterInventory;

	@Getter
	private Sound sound;
	
	public Teleporter(Lobby lobby) {
		super(lobby);
	}

	@Override
	public void enable() {

		this.teleporterInventory = new TeleporterInventory(this);

		loadConfig();

		try {
			this.sound = Sound.valueOf(config.getConfig().getString("Settings.Sound"));
		} catch (Exception ex) {
			logger.error("Couldn't load sound from config. Using NOTE_BASS.");
			this.sound = Sound.NOTE_BASS;
		}

		new EventPlayerInteract(this);
		new EventPlayerJoin(this);
		new EventPlayerInventoryClick(this);
		
		new SubCommandSetLocation(this);
		new SubCommandReload(this);
	}

	public void loadConfig() {

		File from = FileManager.getCachedFile("config.yml", this);
		File to = new File("plugins/Lobby/modules/Teleporter/config.yml");

		to.getParentFile().mkdirs();

		if(!to.exists()) {

			try {
				Files.copy(from.toPath(), to.toPath());
			} catch (IOException e) {
				e.printStackTrace();
			}

		}
		
		this.config = new Configuration(to);

		teleporterInventory.reload();

	}

}
