package de.pauhull.teleporter.waypoint;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.bukkit.inventory.ItemStack;

/**
 * Created by Paul on 06.08.2017.
 */
@AllArgsConstructor
public class Waypoint {

    @Getter
    private ItemStack stack;

    @Getter
    private String id;

    @Getter
    private int slot;

}
