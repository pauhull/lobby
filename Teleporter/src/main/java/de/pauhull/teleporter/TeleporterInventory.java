package de.pauhull.teleporter;

import de.pauhull.lobby.util.Configuration;
import de.pauhull.lobby.util.ItemUtil;
import de.pauhull.teleporter.waypoint.Waypoint;
import lombok.Getter;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.Bukkit;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.List;

public class TeleporterInventory {

	@Getter
	private List<Waypoint> waypoints;

	private Teleporter teleporter;

	TeleporterInventory(Teleporter teleporter) {
		this.teleporter = teleporter;
	}
	
	public Inventory loadInventory() {
		
		Configuration config = teleporter.getConfig();
		
		int slots = config.getConfig().getInt("Settings.TeleporterSlots");
		String title = ChatColor.translateAlternateColorCodes('&', 
				config.getConfig().getString("Settings.Title"));

		Inventory inventory = Bukkit.createInventory(null, slots, title);

		for(Waypoint waypoint : waypoints) {
			inventory.setItem(waypoint.getSlot(), waypoint.getStack());
		}

		return inventory;
		
	}
	
	private List<Waypoint> loadWaypoints() {

		Configuration config = teleporter.getConfig();
		List<Waypoint> waypoints = new ArrayList<>();

		for(String key : config.getConfig().getConfigurationSection("Items").getKeys(false)) {
			ItemStack stack = ItemUtil.readFromConfig(config.getConfig(), "Items." + key);
			int slot = config.getConfig().getInt("Items." + key + ".Slot");
			waypoints.add(new Waypoint(stack, key, slot));
		}
	
		return waypoints;
		
	}

	public void reload() {
		this.waypoints = loadWaypoints();
	}
	
}
