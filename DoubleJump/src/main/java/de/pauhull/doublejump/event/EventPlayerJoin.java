package de.pauhull.doublejump.event;

import de.pauhull.doublejump.DoubleJump;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

public class EventPlayerJoin implements Listener {

    private DoubleJump doubleJump;

    public EventPlayerJoin(DoubleJump doubleJump) {
        this.doubleJump = doubleJump;

        Bukkit.getPluginManager().registerEvents(this, doubleJump.getLobby().getPlugin());
    }

    @EventHandler
    public void onJoin(PlayerJoinEvent event) {
        Player player = event.getPlayer();

        if(player.hasPermission(doubleJump.getPermission())) {
            player.setAllowFlight(true);
        }
    }

}
