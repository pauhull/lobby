package de.pauhull.doublejump.event;

import de.pauhull.doublejump.DoubleJump;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.util.Vector;

import java.util.concurrent.TimeUnit;

public class EventPlayerMove implements Listener {

    private DoubleJump doubleJump;

    public EventPlayerMove(DoubleJump doubleJump) {
        this.doubleJump = doubleJump;

        Bukkit.getPluginManager().registerEvents(this, doubleJump.getLobby().getPlugin());
    }

    @EventHandler
    public void onMove(PlayerMoveEvent event) {
        Player player = event.getPlayer();

        if(doubleJump.getLobby().getCanBuild().contains(player) || !player.hasPermission(doubleJump.getPermission()))
            return;

        if(player.isFlying()) {
            player.setFlying(false);
            player.setAllowFlight(false);
            player.playSound(player.getLocation(), doubleJump.getSound(), 1, 1);

            Vector v = player.getLocation().getDirection().multiply(doubleJump.getVelocity()).setY(doubleJump.getVerticalVelocity());
            player.setVelocity(v);

            doubleJump.getLobby().getScheduledExecutorService().schedule(() -> {

                if(player.isOnline())
                    player.setAllowFlight(true);

            }, doubleJump.getCooldown(), TimeUnit.SECONDS);
        }
    }

}
