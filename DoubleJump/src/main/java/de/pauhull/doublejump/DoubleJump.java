package de.pauhull.doublejump;

import de.pauhull.doublejump.event.EventPlayerJoin;
import de.pauhull.doublejump.event.EventPlayerMove;
import de.pauhull.lobby.Lobby;
import de.pauhull.lobby.module.Module;
import de.pauhull.lobby.util.Configuration;
import lombok.Getter;
import org.bukkit.Sound;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

public class DoubleJump extends Module {

    @Getter
    private Lobby lobby;

    @Getter
    private Configuration config;

    @Getter
    private String permission;

    @Getter
    private int cooldown;

    @Getter
    private double velocity, verticalVelocity;

    @Getter
    private Sound sound;

    public DoubleJump(Lobby lobby) {
        super(lobby);
        this.lobby = lobby;
    }

    @Override
    public void enable() {
        copyConfigFile();

        this.permission = config.getConfig().getString("Permission");
        this.cooldown = config.getConfig().getInt("Cooldown");
        this.velocity = config.getConfig().getDouble("Velocity");
        this.verticalVelocity = config.getConfig().getDouble("VerticalVelocity");

        try {
            this.sound = Sound.valueOf(config.getConfig().getString("Sound"));
        } catch (Exception e) {
            this.sound = Sound.valueOf("ENDERDRAGON_WINGS");
            System.out.println("Couldn't find sound \"" + config.getConfig().getString("Sound") + "\". Using \"ENDERDRAGON_WINGS\" instead.");
        }

        new EventPlayerJoin(this);
        new EventPlayerMove(this);
    }

    private void copyConfigFile() {

        File to = new File("plugins/Lobby/modules/DoubleJump/config.yml");
        to.mkdirs();

        if(!to.exists()) {
            try {
                Files.copy(DoubleJump.class.getClassLoader().getResourceAsStream("config.yml"), to.toPath());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        this.config = new Configuration(to);

    }

}
