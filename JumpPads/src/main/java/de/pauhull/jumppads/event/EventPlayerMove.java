package de.pauhull.jumppads.event;

import de.pauhull.jumppads.JumpPads;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.util.Vector;

/**
 * Created by Paul on 23.06.2017.
 */
public class EventPlayerMove implements Listener {

    private JumpPads jumpPads;

    public EventPlayerMove(JumpPads jumpPads) {
        this.jumpPads = jumpPads;

        Bukkit.getPluginManager().registerEvents(this, jumpPads.getLobby().getPlugin());
    }

    @EventHandler
    public void onMove(PlayerMoveEvent event) {

        Player player = event.getPlayer();
        Location location = player.getLocation();

        if(location.getBlock().getType() == jumpPads.getJumpPadMaterial()) {

            Vector vector = location.getDirection().multiply(jumpPads.getVelocity()).setY(jumpPads.getYVelocity());
            player.setVelocity(vector);
            player.playSound(location, jumpPads.getSound(), 10f, 1f);
            player.playEffect(location, jumpPads.getEffect(), 31);

        }

    }

}
