package de.pauhull.jumppads;

import de.pauhull.jumppads.event.EventPlayerMove;
import de.pauhull.lobby.Lobby;
import de.pauhull.lobby.module.FileManager;
import de.pauhull.lobby.module.Module;
import de.pauhull.lobby.util.Configuration;
import lombok.Getter;
import org.bukkit.Effect;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

/**
 * Created by Paul on 23.06.2017.
 */
public class JumpPads extends Module {

    private static Logger logger = LoggerFactory.getLogger(JumpPads.class);
    private Configuration config;

    @Getter
    private double velocity;

    @Getter
    private double yVelocity;

    @Getter
    private Material jumpPadMaterial;

    @Getter
    private Sound sound;

    @Getter
    private Effect effect;

    public JumpPads(Lobby lobby) {
        super(lobby);
    }

    @Override
    public void enable() {

        // Register Events
        new EventPlayerMove(this);

        // Load config file
        loadConfig();

    }

    private void loadConfig() {

        // Copy config if doesn't exist
        File to = new File("plugins/Lobby/modules/JumpPads/config.yml");
        File from = FileManager.getCachedFile("config.yml", this);
        if (!to.exists()) {

            to.getParentFile().mkdirs();

            try {
                Files.copy(from.toPath(), to.toPath());
            } catch (IOException exception) {
                logger.error("Unable to copy default config! No write permissions?", exception);
                return;
            }
        }

        config = new Configuration(to);

        this.jumpPadMaterial = Material.getMaterial(config.getConfig().getString("General.JumpPadMaterial"));
        this.velocity = config.getConfig().getDouble("General.Velocity");
        this.yVelocity = config.getConfig().getDouble("General.VerticalVelocity");
        this.sound = Sound.valueOf(config.getConfig().getString("General.Sound"));
        this.effect = Effect.valueOf(config.getConfig().getString("General.Particle"));
    }

}
