package de.pauhull.enderpearl;

import de.pauhull.enderpearl.event.EventPlayerInteract;
import de.pauhull.enderpearl.event.EventPlayerJoin;
import de.pauhull.lobby.Lobby;
import de.pauhull.lobby.module.FileManager;
import de.pauhull.lobby.module.Module;
import de.pauhull.lobby.util.Configuration;
import lombok.Getter;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

public class EnderPearl extends Module {

	@Getter
	private Configuration config;

	@Getter
	private int cooldown;

	@Getter
	private int hotbar;

	@Getter
	private String permission;

	@Getter
	private ItemStack waitStack;

	@Getter
	private ItemStack pearlStack;

	public EnderPearl(Lobby lobby) {
		super(lobby);
	}

	@Override
	public void enable() {

		copyConfigFiles();

		this.cooldown = config.getConfig().getInt("Cooldown");
		this.hotbar = config.getConfig().getInt("Hotbar");
		this.permission = config.getConfig().getString("Permission");

		waitStack = new ItemStack(Material.FIREWORK_CHARGE);
		ItemMeta waitMeta = waitStack.getItemMeta();
		waitMeta.setDisplayName(ChatColor.translateAlternateColorCodes('&',
				config.getConfig().getString("Wait"))
				.replaceAll("%TIME%", String.valueOf(cooldown)));
		waitStack.setItemMeta(waitMeta);

		pearlStack = new ItemStack(Material.ENDER_PEARL);
		ItemMeta pearlMeta = pearlStack.getItemMeta();
		pearlMeta.setDisplayName(ChatColor.translateAlternateColorCodes('&',
				config.getConfig().getString("DisplayName")));
		pearlStack.setItemMeta(pearlMeta);

		new EventPlayerJoin(this);
		new EventPlayerInteract(this);
	}

	private void copyConfigFiles() {

		File from = FileManager.getCachedFile("config.yml", this);
		File to = new File("plugins/Lobby/modules/EnderPearl/config.yml");

		to.getParentFile().mkdirs();

		if(!to.exists()) {

			try {
				Files.copy(from.toPath(), to.toPath());
			} catch (IOException e) {
				e.printStackTrace();
			}

		}

		config = new Configuration(to);

	}

}