package de.pauhull.enderpearl.event;

import de.pauhull.enderpearl.EnderPearl;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

import java.util.concurrent.TimeUnit;

/**
 * Created by Paul on 06.08.2017.
 */
public class EventPlayerInteract implements Listener {

    private EnderPearl enderPearl;

    public EventPlayerInteract(EnderPearl enderPearl) {
        this.enderPearl = enderPearl;

        Bukkit.getPluginManager().registerEvents(this, enderPearl.getLobby().getPlugin());
    }

    @EventHandler
    public void onInteract(PlayerInteractEvent event) {
        Player player = event.getPlayer();
        ItemStack stack = event.getItem();

        if(stack == null || stack.getItemMeta() == null || stack.getItemMeta().getDisplayName() == null)
            return;

        if(event.getAction() != Action.RIGHT_CLICK_BLOCK && event.getAction() != Action.RIGHT_CLICK_AIR)
            return;

        if(player.getGameMode() == GameMode.CREATIVE || enderPearl.getLobby().getCanBuild().contains(player))
            return;

        if(stack.equals(enderPearl.getPearlStack())) {

            enderPearl.getLobby().getScheduledExecutorService().schedule(() -> {

                if(player.isOnline())
                    player.getInventory().setItem(enderPearl.getHotbar(), enderPearl.getPearlStack());

            }, 50, TimeUnit.MILLISECONDS);

            enderPearl.getLobby().getScheduledExecutorService().schedule(() -> {

                if(player.isOnline())
                    player.getInventory().setItem(enderPearl.getHotbar(), enderPearl.getPearlStack());

            }, enderPearl.getCooldown(), TimeUnit.SECONDS);
        }
    }

}
