package de.pauhull.enderpearl.event;

import de.pauhull.enderpearl.EnderPearl;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

/**
 * Created by Paul on 25.06.2017.
 */
public class EventPlayerJoin implements Listener {

    private EnderPearl enderPearl;

    public EventPlayerJoin(EnderPearl enderPearl) {
        this.enderPearl = enderPearl;

        Bukkit.getPluginManager().registerEvents(this, enderPearl.getLobby().getPlugin());
    }

    @EventHandler
    public void onJoin(PlayerJoinEvent event) {

        Player player = event.getPlayer();
        if(!player.hasPermission(enderPearl.getPermission())) return;

        player.getInventory().setItem(enderPearl.getHotbar(), enderPearl.getPearlStack());

    }

}
