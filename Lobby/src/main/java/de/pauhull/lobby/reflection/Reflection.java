package de.pauhull.lobby.reflection;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

/**
 * Handles reflection to add compatibility for different Spigot versions.
 * @author pauhull
 */
public class Reflection {

    private static Map<String, Class<?>> classCache = new HashMap<>();

    /**
     * Get specific NMS class.
     * @param classString The name of the class. Has to be in <tt>net.minecraft.server.VERSION</tt>.
     * @return Class with name <tt>classString</tt>.
     * @throws ClassNotFoundException Thrown if class could't be found.
     */
    private static Class<?> getNMSClass(String classString) throws ClassNotFoundException {

        if(classCache.containsKey(classString)) {
            return classCache.get(classString);
        }

        String version = Bukkit.getServer().getClass().getPackage().getName()
                .replace('.', ',').split(",")[3] + ".";

        Class<?> clazz = Class.forName("net.minecraft.server." + version + classString);

        classCache.put(classString, clazz);
        return clazz;

    }

    /**
     * Get NMS connection of specified player.
     * @param player Player to get connection from.
     * @return Returns the player's connection object.
     */
    private static Object getConnection(Player player) throws Throwable {

        Method getHandle = player.getClass().getMethod("getHandle");
        Object nmsPlayer = getHandle.invoke(player);
        Field connection = nmsPlayer.getClass().getField("playerConnection");

        return connection.get(nmsPlayer);

    }

    /**
     * <tt>PacketPlayOutWorldParticles</tt> helper.
     */
    public static class ParticlePacket {

        private Object packet;
        private Method sendPacket;

        /**
         * Constructor for <tt>PacketPlayOutWorldParticles</tt>
         * @param particle EnumParticle to display.
         * @param b IDK just set it to true.
         * @param x X position.
         * @param y Y position.
         * @param z Z position.
         * @param xOffset X offset.
         * @param yOffset Y offset.
         * @param zOffset Z offset.
         * @param data Particle data/speed.
         * @param amount Particle amount.
         * @param ints Integer array, IDK for what.
         */
        public ParticlePacket(String particle, boolean b, float x, float y, float z, float xOffset, float yOffset, float zOffset, float data, int amount, int[] ints) throws Throwable {
            Class<?> packetClass = getNMSClass("PacketPlayOutWorldParticles");
            Class<?> enumParticleClass = getNMSClass("EnumParticle");

            Constructor<?> packetConstructor = packetClass.getConstructor(enumParticleClass, boolean.class, float.class, float.class,
                    float.class, float.class, float.class, float.class, float.class, int.class, int[].class);

            Object enumParticle = enumParticleClass.getMethod("valueOf", String.class).invoke(null, particle);

            this.packet = packetConstructor.newInstance(enumParticle, b, x, y, z, xOffset, yOffset, zOffset, data, amount, ints);
            this.sendPacket = getNMSClass("PlayerConnection").getMethod("sendPacket", getNMSClass("Packet"));
        }

        /**
         * Send packet to player.
         * @param player Player to send packet to.
         */
        public void sendPacket(Player player) throws Throwable {

            sendPacket.invoke(getConnection(player), packet);

        }

    }

}
