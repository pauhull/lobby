package de.pauhull.lobby;

import de.jackwhite20.cyclone.Cyclone;
import de.jackwhite20.cyclone.db.Type;
import de.jackwhite20.cyclone.db.settings.CycloneSettings;
import de.pauhull.lobby.util.Configuration;
import de.pauhull.lobby.util.Locale;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;

public class Main extends JavaPlugin {

    private Lobby lobby;

    @Override
    public void onEnable() {

        Locale locale = new Locale("de_DE", "en_US");
        Configuration config = new Configuration(new File("plugins/" + Lobby.PLUGIN_NAME + "/config.yml"));
        Cyclone cyclone;

        String method = config.getConfig().getString("Database.Method");
        if (method.equalsIgnoreCase("MySQL")) {

            String host = config.getConfig().getString("Database.MySQL.Host");
            int port = config.getConfig().getInt("Database.MySQL.Port");
            String user = config.getConfig().getString("Database.MySQL.User");
            String password = config.getConfig().getString("Database.MySQL.Password");
            String database = config.getConfig().getString("Database.MySQL.Database");

            cyclone = new Cyclone(new CycloneSettings.Builder()
                    .host(host)
                    .port(port)
                    .user(user)
                    .password(password)
                    .database(database)
                    .poolSize(10)
                    .poolName(Lobby.PLUGIN_NAME)
                    .queryTimeout(1000)
                    .build());
        } else {
            if (!method.equalsIgnoreCase("SQLite"))
                Bukkit.getConsoleSender().sendMessage("Invalid data storing method \"" + method + "\". Using SQLite instead.");

            cyclone = new Cyclone(new CycloneSettings.Builder()
                    .database("plugins/" + Lobby.PLUGIN_NAME + "/database.db")
                    .type(Type.SQLITE)
                    .build());
        }

        try {
            cyclone.connect();
        } catch (Exception e) {
            Bukkit.getConsoleSender().sendMessage("§4Critical error while initializing database. Please check your config.yml!");
            Bukkit.getConsoleSender().sendMessage("§4Plugin disabled.");
            e.printStackTrace();
            return;
        }

        this.lobby = new Lobby(locale, config, cyclone, this);

        this.lobby.enable();


    }

    @Override
    public void onDisable() {
        if (this.lobby != null)
            this.lobby.disable();
    }

}
