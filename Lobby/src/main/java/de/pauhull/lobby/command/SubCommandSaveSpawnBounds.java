package de.pauhull.lobby.command;

import de.pauhull.lobby.Lobby;
import de.pauhull.lobby.annotation.SubCommand;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * Saves the spawn bounds.
 * Usage: /lobby savespawnbounds
 * @author pauhull
 */
public class SubCommandSaveSpawnBounds implements SubCommandExecutor {

    private Lobby lobby;

    /**
     * Command constructor. Takes an instance of Lobby.
     * @param lobby Lobby instance.
     */
    public SubCommandSaveSpawnBounds(Lobby lobby) {
        this.lobby = lobby;

        CommandManager.registerSubCommand(this);
    }

    /**
     * Saves the selected spawn bounds.
     * @param sender Command sender.
     * @param args Command arguments.
     * @return Returns true if a response was given.
     */
    @SubCommand(name = "savespawnbounds", commandDescription = "Save spawn bounds", usage = "/lobby savespawnbounds", permission = "lobby.savespawnbounds")
    @Override
    public boolean onSubCommand(CommandSender sender, String[] args) {
        if (!args[0].equalsIgnoreCase("savespawnbounds")) {
            return false;
        }

        Player player;

        if (sender instanceof Player) {
            player = (Player) sender;
        } else {
            sender.sendMessage(lobby.getLocale().getMessage("en_US", "Error.OnlyPlayers", true));
            return true;
        }

        if (player.hasPermission("lobby.savespawnbounds")) {
            if (!lobby.getSpawnBoundsCache().containsKey(player.getName())) {
                lobby.getLocale().sendMessage(player, "Error.BoundsNotSet", true);
                return true;
            }
            if (lobby.getSpawnBoundsCache().get(player.getName())[0] != null && lobby.getSpawnBoundsCache().get(player.getName())[1] != null) {

                lobby.getSpawnBounds()[0] = lobby.getSpawnBoundsCache().get(player.getName())[0];
                lobby.getSpawnBounds()[1] = lobby.getSpawnBoundsCache().get(player.getName())[1];

                lobby.getLocationManager().saveLocation("POS1", lobby.getSpawnBoundsCache().get(player.getName())[0]);
                lobby.getLocationManager().saveLocation("POS2", lobby.getSpawnBoundsCache().get(player.getName())[1]);

                lobby.getLocale().sendMessage(player, "Info.BoundsUpdated", true);
            } else {
                lobby.getLocale().sendMessage(player, "Error.BoundsNotSet", true);
            }
        } else {
            lobby.getLocale().sendMessage(player, "Error.NoPermissions", true);
        }

        return true;
    }

}
