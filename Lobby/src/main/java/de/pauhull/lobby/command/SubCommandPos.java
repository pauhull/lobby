package de.pauhull.lobby.command;

import de.pauhull.lobby.Lobby;
import de.pauhull.lobby.annotation.SubCommand;
import org.bukkit.Location;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * Alternative to wand.
 * Usage: /lobby pos 1|2
 * @author pauhull
 */
public class SubCommandPos implements SubCommandExecutor {

    private Lobby lobby;

    /**
     * Command constructor. Takes an instance of Lobby.
     * @param lobby Instance of Lobby.
     */
    public SubCommandPos(Lobby lobby) {
        this.lobby = lobby;

        CommandManager.registerSubCommand(this);
    }

    /**
     * Caches positions.
     * @param sender Command sender.
     * @param args Command arguments.
     * @return Returns true if a response was given.
     */
    @SubCommand(name = "pos", commandDescription = "Set lobby bounds", usage = "/lobby pos <1|2>", permission = "lobby.pos")
    @Override
    public boolean onSubCommand(CommandSender sender, String[] args) {
        if (!args[0].equalsIgnoreCase("pos")) {
            return false;
        }

        Player player;

        if (sender instanceof Player) {
            player = (Player) sender;
        } else {
            sender.sendMessage(lobby.getLocale().getMessage("en_US", "Error.OnlyPlayers", true));
            return true;
        }

        if (player.hasPermission("lobby.pos")) {
            if (args.length == 2) {
                if (args[1].equalsIgnoreCase("1") || args[1].equalsIgnoreCase("2")) {

                    int i = Integer.valueOf(args[1]);

                    if (lobby.getSpawnBoundsCache().containsKey(player.getName())) {
                        Location[] put = lobby.getSpawnBoundsCache().get(player.getName());
                        put[i - 1] = player.getLocation();
                        lobby.getSpawnBoundsCache().put(player.getName(), put);
                    } else {
                        Location[] put = new Location[2];
                        put[i - 1] = player.getLocation();
                        lobby.getSpawnBoundsCache().put(player.getName(), put);
                    }

                    lobby.getLocale().sendMessage(player, "Info.PositionSet", true, new String[]{"%POS%", args[1]});
                    return true;
                }

                lobby.getLocale().sendMessage(player, "Info.PositionUsage", true);
            } else {
                lobby.getLocale().sendMessage(player, "Info.PositionUsage", true);
            }
        } else {
            lobby.getLocale().sendMessage(player, "Error.NoPermissions", true);
        }

        return true;
    }

}
