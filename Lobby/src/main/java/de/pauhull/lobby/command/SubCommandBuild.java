package de.pauhull.lobby.command;

import de.pauhull.lobby.Lobby;
import de.pauhull.lobby.annotation.SubCommand;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * Command to enable block placing and breaking etc. in lobby
 * Usage: /lobby build
 * @author pauhull
 */
public class SubCommandBuild implements SubCommandExecutor {

    private Lobby lobby;

    /**
     * Command constructor. Takes an instance of Lobby.
     * @param lobby Lobby instance.
     */
    public SubCommandBuild(Lobby lobby) {
        this.lobby = lobby;

        CommandManager.registerSubCommand(this);
    }

    /**
     * Enable or disable building for specified player in lobby.
     * @param sender Command sender.
     * @param args Command arguments.
     * @return Returns true if a response was given.
     */
    @SubCommand(name = "build", commandDescription = "Build in Lobby", usage = "/lobby build", permission = "lobby.build")
    @Override
    public boolean onSubCommand(CommandSender sender, String[] args) {
        if (!args[0].equalsIgnoreCase("build")) {
            return false;
        }

        Player player;

        if (sender instanceof Player) {
            player = (Player) sender;
        } else {
            sender.sendMessage(lobby.getLocale().getMessage("en_US", "Error.OnlyPlayers", true));
            return true;
        }

        if (player.hasPermission("lobby.build")) {
            if (!lobby.getCanBuild().contains(player)) {
                lobby.getCanBuild().add(player);
                lobby.getLocale().sendMessage(player, "Info.CanBuild", true);
            } else {
                lobby.getCanBuild().remove(player);
                lobby.getLocale().sendMessage(player, "Info.CannotBuild", true);
            }
        } else {
            lobby.getLocale().sendMessage(player, "Error.NoPermissions", true);
        }

        return true;
    }

}
