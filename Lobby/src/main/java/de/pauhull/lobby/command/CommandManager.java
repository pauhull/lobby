package de.pauhull.lobby.command;

import de.pauhull.lobby.Lobby;
import de.pauhull.lobby.annotation.SubCommand;
import de.pauhull.lobby.module.Module;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Handles commands.
 * @author pauhull
 */
public class CommandManager implements CommandExecutor {

    private static Map<SubCommandExecutor, SubCommandDescription> executors = new HashMap<>();

    private Lobby lobby;

    /**
     * The constructor takes an instance of Lobby.
     * @param lobby Lobby instance.
     */
    public CommandManager(Lobby lobby) {
        this.lobby = lobby;

        new SubCommandBuild(lobby);
        new SubCommandPos(lobby);
        new SubCommandWand(lobby);
        new SubCommandSaveSpawnBounds(lobby);
        new SubCommandSetSpawn(lobby);

        lobby.getPlugin().getCommand("lobby").setExecutor(this);
    }

    /**
     * Registers a subcommand.
     * <strong>NOTE:</strong> Please add an @SubCommand annotation before subcommand methods so the CommandManager recognizes them.
     * @param executor The Subcommand Executor.
     * @see de.pauhull.lobby.command.SubCommandExecutor
     */
    public static void registerSubCommand(SubCommandExecutor executor) {

        for(Method method : executor.getClass().getDeclaredMethods()) {

            if(method.isAnnotationPresent(SubCommand.class)) {

                Annotation annotation = method.getAnnotation(SubCommand.class);
                SubCommand subCommand = (SubCommand) annotation;
                SubCommandDescription description = new SubCommandDescription(subCommand.name(), subCommand.commandDescription(),
                        subCommand.usage(), subCommand.permission());

                executors.put(executor, description);
            }

        }
    }

    /**
     * Handles subcommands.
     * @param sender Command sender.
     * @param command Command instance.
     * @param label Command label.
     * @param args Command arguments.
     * @return Returns true.
     */
    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {

        String locale = "en_US";

        if(sender instanceof Player) {
            locale = lobby.getLocale().getLanguage((Player)sender);
        }

        if (label.equalsIgnoreCase("lobby")) {
            if (args.length == 0) {
                showHelp(sender);
                return true;
            }

            if (args[0].equalsIgnoreCase("help")) {
                showHelp(sender);
                return true;
            }

            if (args[0].equalsIgnoreCase("modules")) {

                if(sender instanceof Player) {
                    if(!sender.hasPermission("lobby.modules")) {
                        sender.sendMessage(lobby.getLocale().getMessage(locale, "Error.NoPermissions", true));
                        return true;
                    }
                }

                List<Module> modules = lobby.getModuleManager().getModules();

                if(modules.isEmpty()) {
                    sender.sendMessage(lobby.getLocale().getMessage(locale, "Error.NoModules", true));
                } else{
                    sender.sendMessage(lobby.getLocale().getMessage(locale, "Info.ModuleList", true));

                    for(Module module : lobby.getModuleManager().getModules()) {

                        StringBuilder stringBuilder = new StringBuilder();
                        for(String author : module.getDescription().getAuthors()) {
                            stringBuilder.append(stringBuilder.length() == 0 ? "" : ", ").append(author);
                        }
                        String authors = stringBuilder.toString();

                        sender.sendMessage(lobby.getLocale().getMessage(locale, "Info.ModuleFormat", true,
                                new String[]{"%NAME%", module.getDescription().getName()},
                                new String[]{"%VERSION%", module.getDescription().getVersion()},
                                new String[]{"%AUTHORS%", authors}));
                    }
                }

                return true;
            }

            boolean response = false;

            for (SubCommandExecutor executor : executors.keySet()) {
                if (!sender.hasPermission(executors.get(executor).getPermission())) {
                    sender.sendMessage(lobby.getLocale().getMessage(locale, "Error.NoPermissions", true));
                    response = true;
                    continue;
                }
                if (executor.onSubCommand(sender, args)) {
                    response = true;
                }
            }

            if (!response) {
                sender.sendMessage(lobby.getLocale().getMessage(locale, "Error.NotFound", true));
            }
        }

        return true;
    }

    /**
     * Shows help to specified command sender.
     * @param sender Command sender to show help to.
     */
    private void showHelp(CommandSender sender) {

        String locale = "en_US";

        if (sender instanceof Player) {
            Player player = (Player) sender;

            locale = lobby.getLocale().getLanguage(player);

            if (!player.hasPermission("lobby.help")) {
                lobby.getLocale().sendMessage(player, "Error.NoPermissions", true);
                return;
            }
        }

        List<SubCommandDescription> descriptions = new ArrayList<>();

        descriptions.add(new SubCommandDescription("help", "Show help", "/lobby help", "lobby.help"));
        descriptions.add(new SubCommandDescription("modules", "Show all modules", "/lobby modules", "lobby.modules"));

        for (SubCommandExecutor executor : executors.keySet()) {
            descriptions.add(executors.get(executor));
        }

        sender.sendMessage(lobby.getLocale().getMessage(locale, "Info.CommandList", true));
        for (SubCommandDescription description : descriptions) {
            sender.sendMessage(lobby.getLocale().getMessage(locale, "Info.HelpFormat", true,
                    new String[]{"%SYNTAX%", description.getUsage()},
                    new String[]{"%DESCRIPTION%", description.getDescription()},
                    new String[]{"%PERMISSION%", description.getPermission()}));
        }

    }

}
