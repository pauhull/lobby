package de.pauhull.lobby.command;

import de.pauhull.lobby.Lobby;
import de.pauhull.lobby.annotation.SubCommand;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * Sets the spawn location.
 * Usage: /lobby setspawn
 * @author pauhull
 */
public class SubCommandSetSpawn implements SubCommandExecutor {

    private Lobby lobby;

    /**
     * Command constructor. Takes an instance of Lobby.
     * @param lobby Lobby instance.
     */
    public SubCommandSetSpawn(Lobby lobby) {
        this.lobby = lobby;

        CommandManager.registerSubCommand(this);
    }

    /**
     * Sets the spawn location.
     * @param sender Command sender.
     * @param args Command arguments.
     * @return Returns true if a response was given.
     */
    @SubCommand(name = "setspawn", commandDescription = "Set lobby spawn", usage = "/lobby setspawn", permission = "lobby.setspawn")
    @Override
    public boolean onSubCommand(CommandSender sender, String[] args) {
        if (!args[0].equalsIgnoreCase("setspawn")) {
            return false;
        }

        Player player;

        if (sender instanceof Player) {
            player = (Player) sender;
        } else {
            sender.sendMessage(lobby.getLocale().getMessage("en_US", "Error.OnlyPlayers", true));
            return true;
        }

        lobby.setSpawnLocation(player.getLocation());
        lobby.getLocationManager().saveLocation("SPAWNLOCATION", player.getLocation());
        lobby.getLocale().sendMessage(player, "Info.SpawnPointSet", true);

        return true;
    }

}
