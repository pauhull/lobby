package de.pauhull.lobby.command;

import org.bukkit.command.CommandSender;

/**
 * SubCommand interface.
 * Simply implement it and register it in CommandManager to add a subcommand.
 * @author pauhull
 */
public interface SubCommandExecutor {

    /**
     * Gets called when subcommand is executed.
     * @param sender Command sender.
     * @param args Command arguments.
     * @return Returns true if a response was given.
     */
    boolean onSubCommand(CommandSender sender, String[] args);

}
