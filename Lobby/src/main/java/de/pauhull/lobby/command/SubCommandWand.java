package de.pauhull.lobby.command;

import de.pauhull.lobby.Lobby;
import de.pauhull.lobby.annotation.SubCommand;
import org.bukkit.Material;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

/**
 * Gives player the wand item.
 * Usage: /lobby wand
 * @author pauhull
 */
public class SubCommandWand implements SubCommandExecutor {

    private Material wandMaterial;
    private Lobby lobby;

    /**
     * Command constructor. Takes an instance of Lobby.
     * @param lobby Lobby instance
     */
    public SubCommandWand(Lobby lobby) {
        this.wandMaterial = Material.getMaterial(lobby.getConfig().getConfig().getString("General.WandMaterial"));

        this.lobby = lobby;

        CommandManager.registerSubCommand(this);
    }

    /**
     * Gibes the wand item to the player.
     * @param sender Command sender.
     * @param args Command arguments.
     * @return Returns true if a response was given.
     */
    @SubCommand(name = "wand", commandDescription = "Get wand for position setting", usage = "/lobby wand", permission = "lobby.wand")
    @Override
    public boolean onSubCommand(CommandSender sender, String[] args) {
        if (!args[0].equalsIgnoreCase("wand")) {
            return false;
        }

        Player player;

        if (sender instanceof Player) {
            player = (Player) sender;
        } else {
            sender.sendMessage(lobby.getLocale().getMessage("en_US", "Error.OnlyPlayers", true));
            return true;
        }

        if (player.hasPermission("lobby.wand")) {
            player.getInventory().addItem(new ItemStack(wandMaterial));

            lobby.getLocale().sendMessage(player, "Info.PositionSetting", true);
            lobby.getLocale().sendMessage(player, "Info.PositionSettingAlternative", true);
        } else {
            lobby.getLocale().sendMessage(player, "Error.NoPermissions", true);
        }

        return true;
    }

}
