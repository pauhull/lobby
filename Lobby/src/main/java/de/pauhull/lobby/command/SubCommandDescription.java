package de.pauhull.lobby.command;

import lombok.Getter;

/**
 * Description of subcommand.
 * @author pauhull
 */
public class SubCommandDescription {

    /**
     * Subcommand name (e.g. <tt>setspawn</tt>).
     */
    @Getter
    private String name;

    /**
     * Subcommand description (e.g. <tt>Sets the lobby spawn</tt>).
     */
    @Getter
    private String description;

    /**
     * Subcommand usage (e.g. <tt>/lobby setspawn</tt>)
     */
    @Getter
    private String usage;

    /**
     * Subcommand permission (e.g. <tt>lobby.setspawn</tt>).
     */
    @Getter
    private String permission;

    /**
     * The constructor takes all args.
     * @param name Subcommand name.
     * @param description Subcommand description.
     * @param usage Subcommand usage.
     * @param permission Subcommand permission.
     */
    public SubCommandDescription(String name, String description, String usage, String permission) {
        this.name = name;
        this.description = description;
        this.usage = usage;
        this.permission = permission;
    }

}
