package de.pauhull.lobby.module;

import de.pauhull.lobby.Lobby;
import de.pauhull.lobby.util.Configuration;

import java.io.*;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

/**
 * Loads and manages modules.
 * @author pauhull
 */
public class ModuleManager {

    private Lobby lobby;

    private List<Module> modules = new ArrayList<>();

    /**
     * Constructor of ModuleManager. Takes a Lobby instance.
     * @param lobby Instance of Lobby.
     * @see de.pauhull.lobby.Lobby
     */
    public ModuleManager(Lobby lobby) {
        this.lobby = lobby;
    }

    /**
     * Loads all modules.
     */
    public void loadModules() {
        File moduleFolder = new File("plugins/Lobby/modules");

        if (!moduleFolder.exists()) {
            moduleFolder.mkdirs();
        }

        File[] fileList = moduleFolder.listFiles();

        if(fileList == null) return;

        for (File moduleFile : fileList) {

            if (!moduleFile.getName().endsWith(".jar")) {
                continue;
            }

            String pathToJar = moduleFile.getAbsolutePath();

            Configuration config;
            ModuleDescription description = new ModuleDescription();

            try {

                File tempFile;
                JarFile jarFile;
                InputStream inputStream;
                jarFile = new JarFile(moduleFile);
                JarEntry entry = jarFile.getJarEntry("module.yml");

                if (entry == null) {
                    System.err.println("Couldn't load \"module.yml\" of file \"" + moduleFile.getName() + "\".");
                    break;
                }

                inputStream = jarFile.getInputStream(entry);

                tempFile = File.createTempFile(moduleFile.getName().substring(0, moduleFile.getName().length() - 4), ".tmp");
                OutputStream out = new FileOutputStream(tempFile);
                int read;
                byte[] bytes = new byte[1024];

                while ((read = inputStream.read(bytes)) != -1) {
                    out.write(bytes, 0, read);
                }

                config = new Configuration(tempFile);

                List<String> authorList = config.getConfig().getStringList("author");
                String[] authors = new String[authorList.size()];
                int i = 0;
                for(String author : authorList) {
                    authors[i++] = author;
                }

                description.setName(config.getConfig().getString("name"));
                description.setVersion(config.getConfig().getString("version"));
                description.setMain(config.getConfig().getString("main"));
                description.setAuthors(authors);

                tempFile.deleteOnExit();

            } catch (IOException e) {
                System.err.println("Couldn't load module of file \"" + moduleFile.getName() + "\".");
                continue;
            }

            String mainClassName = description.getMain();
            Class<?> mainClass = null;

            try {

                JarFile jarFile = new JarFile(pathToJar);
                Enumeration<JarEntry> entries = jarFile.entries();

                URL[] urls = {new URL("jar:file:" + pathToJar + "!/")};
                URLClassLoader classLoader = new URLClassLoader(urls, this.getClass().getClassLoader());

                while (entries.hasMoreElements()) {

                    JarEntry entry = entries.nextElement();

                    if (entry.isDirectory()) {
                        continue;
                    }

                    InputStream inputStream = jarFile.getInputStream(entry);

                    File tempFile = File.createTempFile(entry.getName(), ".tmp");
                    OutputStream out = new FileOutputStream(tempFile);
                    int read;
                    byte[] bytes = new byte[1024];

                    while ((read = inputStream.read(bytes)) != -1) {
                        out.write(bytes, 0, read);
                    }

                    FileManager.addFileToCache(description.getName() + "_" + entry.getName(), tempFile);

                    if (!entry.getName().endsWith(".class")) continue;

                    String className = entry.getName().substring(0, entry.getName().length() - 6) // foobar[.class] <- 6 letters
                            .replace('/', '.');

                    Class<?> clazz = classLoader.loadClass(className);

                    if (className.equalsIgnoreCase(mainClassName)) {
                        mainClass = clazz;
                    }
                }

            } catch (IOException e) {

                System.err.println("Coudln't find file \"" + pathToJar + "\"");
                continue;

            } catch (ClassNotFoundException e) {

                System.err.println("Couldn't fin Main class in \"" + pathToJar + "\". PLease restart the server.");
                continue;

            }


            if (mainClass != null) {

                try {

                    Module module = (Module) mainClass.getConstructor(Lobby.class).newInstance(lobby);
                    module.setDescription(description);
                    modules.add(module);
                    module.enable();

                } catch (Exception e) {

                    System.err.println("Couldn't initialize class \"" + pathToJar + "\"");
                    e.printStackTrace();

                }

            }

        }
    }

    /**
     * Get all loaded modules.
     * @return List of all loaded modules
     */
    public List<Module> getModules() {
        return modules;
    }

}
