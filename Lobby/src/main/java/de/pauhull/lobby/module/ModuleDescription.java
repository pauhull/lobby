package de.pauhull.lobby.module;

import lombok.Getter;
import lombok.Setter;

/**
 * Module description.
 * @author pauhull
 */
public class ModuleDescription {

    /**
     * Module name.
     */
    @Getter
    @Setter
    private String name;

    /**
     * Module version.
     */
    @Getter
    @Setter
    private String version;

    /**
     * Module main class.
     */
    @Getter
    @Setter
    private String main;

    /**
     * List of all authors.
     */
    @Getter
    @Setter
    private String[] authors;

}
