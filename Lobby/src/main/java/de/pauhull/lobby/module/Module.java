package de.pauhull.lobby.module;

import de.pauhull.lobby.Lobby;
import lombok.Getter;
import lombok.Setter;

/**
 * Abstract module.
 * @author pauhull
 */
public abstract class Module {

    @Getter
    protected Lobby lobby;

    /**
     * Module description.
     * @see de.pauhull.lobby.module.ModuleDescription
     */
    @Getter
    @Setter
    protected ModuleDescription description;

    /**
     * Module constructor
     * @param lobby Instance of Lobby.
     */
    public Module(Lobby lobby) {
        this.lobby = lobby;
    }

    /**
     * Gets called when module is enabled.
     */
    public abstract void enable();

    /**
     * Gets called when module is disabled.
     */
    public void disable() {}

}
