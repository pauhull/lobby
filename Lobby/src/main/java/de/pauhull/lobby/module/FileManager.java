package de.pauhull.lobby.module;

import java.io.File;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Manages files so you can access them in modules.
 * @author pauhull
 */
public class FileManager {

    /**
     * All cached files.
     */
    private static Map<String, File> cachedFiles = new HashMap<>();

    /**
     * Gets specific cached file.
     * @param file File name.
     * @param module Module in which file name originally was.
     * @return Temp file.
     */
    public static File getCachedFile(String file, Module module) {
        return cachedFiles.get(module.getDescription().getName() + "_" + file);
    }

    /**
     * Adds file to the cache.
     * @param name File name.
     * @param file File.
     */
    public static void addFileToCache(String name, File file) {
        cachedFiles.put(name, file);
    }

    /**
     * Clears the whole cache.
     */
    public static void clearCache() {
        Iterator<String> iterator = cachedFiles.keySet().iterator();

        while (iterator.hasNext()) {
            String key = iterator.next();

            if (cachedFiles.get(key).exists()) {
                cachedFiles.get(key).delete();
            }

            iterator.remove();
        }

        for (String key : cachedFiles.keySet()) {
            if (cachedFiles.get(key).exists()) {
                cachedFiles.get(key).delete();
            }
            cachedFiles.remove(key);
        }
    }

    /**
     * Get cached files.
     * @return Map of all cached files with file name and file.
     */
    public static Map<String, File> getCache() {
        return cachedFiles;
    }

}
