package de.pauhull.lobby.location;

import de.jackwhite20.cyclone.db.DBResult;
import de.jackwhite20.cyclone.db.DBRow;
import de.jackwhite20.cyclone.db.serialization.Condition;
import de.jackwhite20.cyclone.query.core.CreateQuery;
import de.jackwhite20.cyclone.query.core.InsertQuery;
import de.jackwhite20.cyclone.query.core.SelectQuery;
import de.jackwhite20.cyclone.query.core.UpdateQuery;
import de.pauhull.lobby.Lobby;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;

import java.util.function.Consumer;

/**
 * Handles spawn location and spawn bounds.
 * @author pauhull
 */
public class LocationManager {

    private Lobby lobby;
    private String tableName;

    /**
     * Constructor of LocationManager. Takes an instance of Lobby.
     * @param lobby Instance of Lobby
     */
    public LocationManager(Lobby lobby) {
        this.lobby = lobby;

        String tablePrefix = lobby.getConfig().getConfig().getString("Database.TablePrefix");
        tableName = tablePrefix + "locations";

        lobby.getCyclone().execute(new CreateQuery.Builder()
                .create(tableName)
                .ifNotExists(true)
                .primaryKey("id")
                .value("id", "INT", "auto_increment")
                .value("identifier", "VARCHAR(255)")
                .value("world", "VARCHAR(255)")
                .value("x", "DOUBLE")
                .value("y", "DOUBLE")
                .value("z", "DOUBLE")
                .value("yaw", "FLOAT")
                .value("pitch", "FLOAT")
                .build());

    }

    /**
     * Get location from database with identifier.
     * @param identifier Identifier of location (e.g. <tt>SPAWNLOCATION</tt>).
     * @param consumer Returns the location.
     */
    public void getLocation(String identifier, Consumer<Location> consumer) {

        lobby.getExecutorService().execute(() -> {

            DBResult result = lobby.getCyclone().query(new SelectQuery.Builder()
                    .select("*")
                    .from(tableName)
                    .where(new Condition("identifier", Condition.Operator.EQUAL, identifier))
                    .build());

            for (DBRow row : result.rows()) {
                World world = Bukkit.getWorld(row.get("world") + "");
                double x = row.get("x");
                double y = row.get("y");
                double z = row.get("z");
                float yaw = row.get("yaw");
                float pitch = row.get("pitch");

                Location location = new Location(world, x, y, z, yaw, pitch);
                consumer.accept(location);
                return;
            }

            consumer.accept(null);

        });

    }

    /**
     * Saves location into database with specific identifier.
     * @param identifier Identifier to save location to database with.
     * @param location Location to save to database.
     */
    public void saveLocation(String identifier, Location location) {

        exists(identifier, (exists) -> {

            if (exists) {
                lobby.getCyclone().execute(new UpdateQuery.Builder()
                        .update(tableName)
                        .where(new Condition("identifier", Condition.Operator.EQUAL, identifier))
                        .set("world", location.getWorld().getName())
                        .set("x", location.getX() + "")
                        .set("y", location.getY() + "")
                        .set("z", location.getZ() + "")
                        .set("yaw", location.getYaw() + "")
                        .set("pitch", location.getPitch() + "")
                        .build());
            } else {
                lobby.getCyclone().execute(new InsertQuery.Builder()
                        .into(tableName)
                        .values("0", identifier, location.getWorld().getName(), location.getX() + "", location.getY() + "",
                                location.getZ() + "", location.getYaw() + "",
                                location.getPitch() + "").build());
            }

        });

    }

    /**
     * Checks if the identifier is already registered in the database.
     * @param identifier Identifier to check.
     * @param consumer Boolean consumer that returns true if the identifier already exists in the database.
     */
    public void exists(String identifier, Consumer<Boolean> consumer) {

        lobby.getExecutorService().execute(() -> {

            DBResult result = lobby.getCyclone().query(new SelectQuery.Builder()
                    .select("*")
                    .from(tableName)
                    .where(new Condition("identifier", Condition.Operator.EQUAL, identifier))
                    .build());

            consumer.accept(result.size() > 0);

        });

    }

}
