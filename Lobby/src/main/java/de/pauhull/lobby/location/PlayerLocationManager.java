package de.pauhull.lobby.location;

import de.jackwhite20.cyclone.db.DBResult;
import de.jackwhite20.cyclone.db.DBRow;
import de.jackwhite20.cyclone.db.serialization.Condition;
import de.jackwhite20.cyclone.query.core.CreateQuery;
import de.jackwhite20.cyclone.query.core.InsertQuery;
import de.jackwhite20.cyclone.query.core.SelectQuery;
import de.jackwhite20.cyclone.query.core.UpdateQuery;
import de.pauhull.lobby.Lobby;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

import java.util.UUID;
import java.util.function.Consumer;

/**
 * Handles locations of players (e.g. when they log out and join back in)
 * @author pauhull
 */
public class PlayerLocationManager implements Listener {

    private Lobby lobby;
    private String tableName;

    /**
     * Constructor of <tt>PlayerLocationManager</tt>. Takes an instance of Lobby.
     * @param lobby Lobby instance.
     */
    public PlayerLocationManager(Lobby lobby) {
        this.lobby = lobby;

        String tablePrefix = lobby.getConfig().getConfig().getString("Database.TablePrefix");
        tableName = tablePrefix + "player_locations";

        lobby.getCyclone().execute(new CreateQuery.Builder()
                .create(tableName)
                .ifNotExists(true)
                .primaryKey("id")
                .value("id", "INT", "auto_increment")
                .value("uuid", "VARCHAR(255)")
                .value("world", "VARCHAR(255)")
                .value("x", "DOUBLE")
                .value("y", "DOUBLE")
                .value("z", "DOUBLE")
                .value("yaw", "FLOAT")
                .value("pitch", "FLOAT")
                .value("timestamp", "BIGINT")
                .build());

        Bukkit.getPluginManager().registerEvents(this, lobby.getPlugin());
    }

    /**
     * Gets the location of a specified uuid.
     * @param uuid UUID of player.
     * @param consumer Consumer which returns the location.
     */
    public void getLocation(UUID uuid, Consumer<Location> consumer) {

        lobby.getExecutorService().execute(() -> {

            DBResult result = lobby.getCyclone().query(new SelectQuery.Builder()
                    .select("*")
                    .from(tableName)
                    .where(new Condition("uuid", Condition.Operator.EQUAL, uuid.toString()))
                    .build());

            for (DBRow row : result.rows()) {
                World world = Bukkit.getWorld(row.get("world") + "");
                double x = row.get("x");
                double y = row.get("y");
                double z = row.get("z");
                float yaw = row.get("yaw");
                float pitch = row.get("pitch");

                Location location = new Location(world, x, y, z, yaw, pitch);

                consumer.accept(location);
            }

        });

    }

    public void getTimestamp(UUID uuid, Consumer<Long> consumer) {

        lobby.getExecutorService().execute(() -> {

            DBResult result = lobby.getCyclone().query(new SelectQuery.Builder()
                    .select("*")
                    .from(tableName)
                    .where(new Condition("uuid", Condition.Operator.EQUAL, uuid.toString()))
                    .build());

            for (DBRow row : result.rows()) {
                consumer.accept(row.get("timestamp"));
            }

        });

    }

    /**
     * Saves location to database.
     * @param uuid UUID of player.
     * @param location Location to save to database.
     */
    public void saveLocation(UUID uuid, Location location) {

        exists(uuid, (exists) -> {

            if (exists) {
                lobby.getCyclone().execute(new UpdateQuery.Builder()
                        .update(tableName)
                        .where(new Condition("uuid", Condition.Operator.EQUAL, uuid.toString()))
                        .set("world", location.getWorld().getName())
                        .set("x", location.getX() + "")
                        .set("y", location.getY() + "")
                        .set("z", location.getZ() + "")
                        .set("yaw", location.getYaw() + "")
                        .set("pitch", location.getPitch() + "")
                        .set("timestamp", System.currentTimeMillis() + "")
                        .build());
            } else {
                lobby.getCyclone().execute(new InsertQuery.Builder()
                        .into(tableName)
                        .values("0", uuid.toString(), location.getWorld().getName(), location.getX() + "",
                                location.getY() + "", location.getZ() + "", location.getYaw() + "",
                                location.getPitch() + "", System.currentTimeMillis() + "").build());
            }

        });

    }

    /**
     * Checks if UUID exists in database already.
     * @param uuid UUID of player.
     * @param consumer Returns true if the UUID exists already.
     */
    public void exists(UUID uuid, Consumer<Boolean> consumer) {

        lobby.getExecutorService().execute(() -> {

            DBResult result = lobby.getCyclone().query(new SelectQuery.Builder()
                    .select("*")
                    .from(tableName)
                    .where(new Condition("uuid", Condition.Operator.EQUAL, uuid.toString()))
                    .build());

            consumer.accept(result.size() > 0);

        });

    }

    /**
     * Join Handler. Player gets teleported to the place he last logged out (multi-server)
     * @param event Activated de.pauhull.doublejump.event
     */
    @EventHandler
    public void onJoin(PlayerJoinEvent event) {
        Player player = event.getPlayer();

        if(lobby.getSpawnLocation() == null)
            return;

        getTeleport(player.getUniqueId(), player::teleport);

    }

    private void getTeleport(UUID uuid, Consumer<Location> consumer) {
        getTimestamp(uuid, timestamp -> {
            if(System.currentTimeMillis() - timestamp < 30*60*1000) {
                getLocation(uuid, location -> consumer.accept(location != null ? location : lobby.getSpawnLocation()));
            } else {
                consumer.accept(lobby.getSpawnLocation());
            }
        });
    }

}
