package de.pauhull.lobby.util;

import lombok.Getter;
import org.bukkit.Bukkit;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.plugin.EventExecutor;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * Quick de.pauhull.doublejump.event canceller.
 * @author pauhull
 */
public class EventCanceller {

    @Getter
    private Condition condition;

    @Getter
    private Class<? extends Event> eventClass;

    @Getter
    private JavaPlugin plugin;

    /**
     * EventCanceller constructor. Please use <tt>EventCanceller#cancelEvent</tt>.
     * @param clazz Event class to cancel.
     * @param plugin JavaPlugin instance.
     * @param condition Cancel condition.
     */
    private EventCanceller(Class<? extends Event> clazz, JavaPlugin plugin, Condition condition) {
        this.eventClass = clazz;
        this.plugin = plugin;
        this.condition = condition;

        Listener emptyListener = new Listener() {};
        EventExecutor executor = (listener, event) -> cancelEvent(event, condition);

        Bukkit.getPluginManager().registerEvent(clazz, emptyListener, EventPriority.NORMAL, executor, plugin);
    }

    /**
     * Cancel de.pauhull.doublejump.event with condition.
     * @param clazz Event class to cancel.
     * @param plugin JavaPlugin instance.
     * @param condition Cancel condition.
     * @return Returns the created EventCanceller instance.
     */
    public static EventCanceller cancelEvent(Class<? extends Event> clazz, JavaPlugin plugin, Condition condition) {
        return new EventCanceller(clazz, plugin, condition);
    }

    /**
     * Cancel de.pauhull.doublejump.event without a condition.
     * @param clazz Event class to cancel.
     * @param plugin JavaPlugin instance.
     * @return Returns the created EventCanceller instance.
     */
    public static EventCanceller cancelEvent(Class<? extends Event> clazz, JavaPlugin plugin) {
        return new EventCanceller(clazz, plugin, null);
    }

    /**
     * Private <tt>cancelEvent</tt> method.
     * @param event Event class to cancel.
     * @param condition Cancel condition (<tt>null</tt> to ignore)
     */
    private void cancelEvent(Event event, Condition condition) {
        if (event instanceof Cancellable) {

            boolean cancel = true;

            if (condition != null)
                cancel = condition.get(event);

            ((Cancellable) event).setCancelled(cancel);
        }
    }

    public interface Condition {
        /**
         * Returns a boolean.
         * @param event Event that gets called.
         * @return Returns if de.pauhull.doublejump.event gets cancelled.
         */
        boolean get(Event event);
    }

}
