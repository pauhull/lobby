package de.pauhull.lobby.util;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Paul on 05.08.2017.
 */
public class ItemUtil {

    public static ItemStack readFromConfig(FileConfiguration config, String key) {

        List<String> lore = new ArrayList<>();
        String displayName = "";
        Material material = Material.STONE;
        int amount = 1;
        short itemData = 0;

        if(config.isSet(key + ".Lore"))
            for(String s : config.getStringList(key + ".Lore"))
                lore.add(ChatColor.translateAlternateColorCodes('&', s));

        if(config.isSet(key + ".Material")) material = Material.valueOf(config.getString(key + ".Material"));
        if(config.isSet(key + ".Amount")) amount = config.getInt(key + ".Amount");
        if(config.isSet(key + ".Data")) itemData = (short) config.getInt(key + ".Data");
        if(config.isSet(key + ".DisplayName")) displayName = ChatColor.translateAlternateColorCodes('&',
                config.getString(key + ".DisplayName"));

        ItemStack stack = new ItemStack(material, amount, itemData);
        ItemMeta meta = stack.getItemMeta();
        meta.setDisplayName(displayName);
        meta.setLore(lore);
        stack.setItemMeta(meta);

        return stack;

    }

}
