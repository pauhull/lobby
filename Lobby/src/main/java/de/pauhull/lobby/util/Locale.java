package de.pauhull.lobby.util;

import de.pauhull.lobby.Lobby;
import de.pauhull.lobby.Main;
import lombok.Getter;
import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;
import java.nio.file.Files;
import java.util.HashMap;
import java.util.Map;

public class Locale {

    @Getter
    private String[] locales;

    @Getter
    private Map<String, FileConfiguration> languageConfigs;

    public Locale(String... locales) {
        this.locales = locales;
        copyFiles();
    }

    private void copyFiles() {

        this.languageConfigs = new HashMap<>();

        for (String file : locales) {

            File localeFile = new File("plugins/" + Lobby.PLUGIN_NAME + "/locale", file + ".yml");

            localeFile.getParentFile().mkdirs();

            if (!localeFile.exists()) {
                try {
                    Files.copy(Main.class.getClassLoader().getResourceAsStream("locale/" + file + ".yml"), localeFile.toPath());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            this.languageConfigs.put(file, YamlConfiguration.loadConfiguration(localeFile));

        }

    }

    public void sendMessage(Player player, String location, boolean withPrefix, String[]... replacements) {

        String language = getLanguage(player);
        String message = getMessage(language, location, withPrefix, replacements);
        player.sendMessage(message);

    }

    public String getMessage(String language, String location, boolean withPrefix, String[]... replacements) {

        String message;

        if (languageConfigs.containsKey(language)) {
            message = languageConfigs.get(language).getString(location);
        } else {
            message = languageConfigs.get("en_US").getString(location);
        }

        message = ChatColor.translateAlternateColorCodes('&', message);

        for (String[] replacement : replacements) {
            if (replacement.length < 2) {
                break;
            }

            message = message.replaceAll(replacement[0], replacement[1]);
        }

        if(withPrefix)
            message = getMessage(language, "Prefix", false) + message;

        return message;

    }


    public String getLanguage(Player player) {
        try {
            Object handle = player.getClass().getMethod("getHandle").invoke(player);
            Field locale = handle.getClass().getDeclaredField("locale");
            locale.setAccessible(true);
            return (String) locale.get(handle);
        } catch (Exception e) {
            return "en_US";
        }
    }

}
