package de.pauhull.lobby.util;

import org.bukkit.Location;
import org.bukkit.util.Vector;

/**
 * Util class for vector calculation.
 */
public class Util {

    /**
     * Calculate vector from <tt>loc1</tt> to <tt>loc2</tt>.
     * @param loc1 First parameter.
     * @param loc2 Second parameter.
     * @return Get vector between <tt>loc1</tt> and <tt>loc2</tt>.
     */
    public static Vector calculateVector(Location loc1, Location loc2) {
        double x1 = loc1.getX();
        double y1 = loc1.getY();
        double z1 = loc1.getZ();

        double x2 = loc2.getX();
        double y2 = loc2.getY();
        double z2 = loc2.getZ();

        double dX = x1 - x2;
        double dY = y1 - y2;
        double dZ = z1 - z2;

        return new Vector(dX, dY, dZ);
    }

    /**
     * Check if <tt>loc</tt> is in bounds of <tt>loc1</tt> and <tt>loc2</tt>.
     * @param loc First parameter.
     * @param loc1 Second parameter.
     * @param loc2 Third parameter.
     * @return Check if is in bounds.
     */
    public static boolean inBounds(Location loc, Location loc1, Location loc2) {
        double x = loc.getX();
        double y = loc.getY();
        double z = loc.getZ();

        double x1 = loc1.getX();
        double y1 = loc1.getY();
        double z1 = loc1.getZ();

        double x2 = loc2.getX();
        double y2 = loc2.getY();
        double z2 = loc2.getZ();

        double minX = Math.min(x1, x2);
        double minY = Math.min(y1, y2);
        double minZ = Math.min(z1, z2);

        double maxX = Math.max(x1, x2);
        double maxY = Math.max(y1, y2);
        double maxZ = Math.max(z1, z2);

        if (x < maxX && x > minX && y < maxY && y > minY && z < maxZ && z > minZ) {
            return true;
        }

        return false;
    }

    /**
     * Check if <tt>loc</tt> is in bounds of <tt>loc1</tt> and <tt>loc2</tt> with <tt>buffer</tt>.
     * @param loc First parameter.
     * @param loc1 Second parameter.
     * @param loc2 Third parameter.
     * @param buffer Buffer.
     * @return Check if is in bounds with buffer.
     */
    public static boolean inBounds(Location loc, Location loc1, Location loc2, double buffer) {
        double x = loc.getX();
        double y = loc.getY();
        double z = loc.getZ();

        double x1 = loc1.getX();
        double y1 = loc1.getY();
        double z1 = loc1.getZ();

        double x2 = loc2.getX();
        double y2 = loc2.getY();
        double z2 = loc2.getZ();

        double minX = Math.min(x1, x2) - buffer;
        double minY = Math.min(y1, y2) - buffer;
        double minZ = Math.min(z1, z2) - buffer;

        double maxX = Math.max(x1, x2) + buffer;
        double maxY = Math.max(y1, y2) + buffer;
        double maxZ = Math.max(z1, z2) + buffer;

        if (x < maxX && x > minX && y < maxY && y > minY && z < maxZ && z > minZ) {
            return true;
        }

        return false;
    }

}
