package de.pauhull.lobby;

import de.jackwhite20.cyclone.Cyclone;
import de.pauhull.lobby.command.CommandManager;
import de.pauhull.lobby.event.*;
import de.pauhull.lobby.location.LocationManager;
import de.pauhull.lobby.location.PlayerLocationManager;
import de.pauhull.lobby.module.FileManager;
import de.pauhull.lobby.module.Module;
import de.pauhull.lobby.module.ModuleManager;
import de.pauhull.lobby.util.Configuration;
import de.pauhull.lobby.util.EventCanceller;
import de.pauhull.lobby.util.Locale;
import lombok.Getter;
import lombok.Setter;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.block.BlockBurnEvent;
import org.bukkit.event.block.BlockIgniteEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.event.entity.EntitySpawnEvent;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;
import org.bukkit.event.player.PlayerPortalEvent;
import org.bukkit.event.weather.WeatherChangeEvent;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * The Lobby main class handles the registration of commands, events and other classes.
 * @author pauhull
 */
public class Lobby {

    public static final String PLUGIN_NAME = "Lobby";

    @Getter
    private LocationManager locationManager;

    @Getter
    private PlayerLocationManager playerLocationManager;

    @Getter
    private ModuleManager moduleManager;

    @Getter
    private Locale locale;

    @Setter
    @Getter
    private Location spawnLocation;

    @Getter
    private Location[] spawnBounds = new Location[2];

    @Getter
    private Map<String, Location[]> spawnBoundsCache = new HashMap<>();

    @Getter
    private List<Player> canBuild = new ArrayList<>();

    @Getter
    private Configuration config;

    @Getter
    private JavaPlugin plugin;

    @Getter
    private ScheduledExecutorService scheduledExecutorService;

    @Getter
    private ExecutorService executorService;

    @Getter
    private Cyclone cyclone;

    @Getter
    private static CommandManager commandManager;

    @Getter
    private static Lobby instance;

    @Getter
    private AtomicInteger threadID = new AtomicInteger(0);

    /**
     * Creates new Lobby instance with a config and a JavaPlugin.
     * @param config The main config of the plugin
     * @param plugin Bukkit Plugin
     */
    public Lobby(Locale locale, Configuration config, Cyclone cyclone, JavaPlugin plugin) {
        instance = this;

        this.locale = locale;
        this.config = config;
        this.cyclone = cyclone;
        this.plugin = plugin;
        this.scheduledExecutorService = Executors.newSingleThreadScheduledExecutor(r -> {
            Thread thread = new Thread(r);
            thread.setName(PLUGIN_NAME + " Task #" + threadID.getAndIncrement());
            return thread;
        });
        this.executorService = Executors.newCachedThreadPool(r -> {
            Thread thread = new Thread(r);
            thread.setName("Executor Service");
            return thread;
        });
    }

    /**
     * Gets called when the plugin starts.
     * @see de.pauhull.lobby.Main
     */
    public void enable() {

        locationManager = new LocationManager(this);
        loadLocations();

        System.out.println("Locations loaded");

        EventCanceller.cancelEvent(BlockPlaceEvent.class, plugin, event -> !canBuild.contains(((BlockPlaceEvent) event).getPlayer()));
        EventCanceller.cancelEvent(PlayerPortalEvent.class, plugin, event -> !canBuild.contains(((PlayerPortalEvent) event).getPlayer()));
        EventCanceller.cancelEvent(PlayerDropItemEvent.class, plugin, event -> !canBuild.contains(((PlayerDropItemEvent) event).getPlayer()));
        EventCanceller.cancelEvent(PlayerPickupItemEvent.class, plugin, event -> !canBuild.contains(((PlayerPickupItemEvent) event).getPlayer()));
        EventCanceller.cancelEvent(InventoryClickEvent.class, plugin, event -> !canBuild.contains(((InventoryClickEvent)event).getWhoClicked()));
        EventCanceller.cancelEvent(WeatherChangeEvent.class, plugin, event -> ((WeatherChangeEvent) event).toWeatherState());
        EventCanceller.cancelEvent(BlockBurnEvent.class, plugin);
        EventCanceller.cancelEvent(BlockIgniteEvent.class, plugin);
        EventCanceller.cancelEvent(EntityDamageEvent.class, plugin);
        EventCanceller.cancelEvent(FoodLevelChangeEvent.class, plugin);
        EventCanceller.cancelEvent(EntitySpawnEvent.class, plugin);

        EventCanceller.cancelEvent(EntityExplodeEvent.class, plugin, event -> {
            ((EntityExplodeEvent) event).blockList().clear();
            return true;
        });

        new EventBlockBreak(this);
        new EventPlayerInteract(this);
        new EventPlayerJoin(this);
        new EventPlayerMove(this);
        new EventPlayerQuit(this);
        new EventCropTrample(this);

        System.out.println("Listeners loaded");

        commandManager = new CommandManager(this);

        System.out.println("Commands loaded");

        moduleManager = new ModuleManager(this);
        moduleManager.loadModules();

        System.out.println("Modules loaded");

        this.playerLocationManager = new PlayerLocationManager(this);

        System.out.println("Player Location Manager loaded");

        Bukkit.getConsoleSender().sendMessage("§aLobby Plugin successfully started!");

    }

    /**
     * Loads spawn location and spawn bounds.
     */
    public void loadLocations() {

        locationManager.getLocation("SPAWNLOCATION", (spawnLocation) -> this.spawnLocation = spawnLocation);

        locationManager.getLocation("POS1", (pos1) -> locationManager.getLocation("POS2", (pos2) -> this.spawnBounds = new Location[]{pos1, pos2}));

    }

    /**
     * Gets Called when the plugin stops.
     * @see de.pauhull.lobby.Main
     */
    void disable() {

        scheduledExecutorService.shutdown();
        executorService.shutdown();

        try {
            cyclone.close();
        } catch(Exception ignore) {

        }

        FileManager.clearCache();

        if (moduleManager == null) return;

        for (Module module : moduleManager.getModules()) {

            module.disable();

        }

    }

}