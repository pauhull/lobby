package de.pauhull.lobby.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Annotation that needs to be placed before subcommand methods to be recognized.
 * @author pauhull
 */

@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface SubCommand {

    /**
     * Subcommand name.
     * @return Returns the subcommand name.
     */
    String name();

    /**
     * Subcommand description.
     * @return Returns the subcommand description.
     */
    String commandDescription();

    /**
     * Subcommand usage.
     * @return Returns the subcommand usage.
     */
    String usage();

    /**
     * Subcommand permission.
     * @return Returns the subcommand permission.
     */
    String permission();

}
