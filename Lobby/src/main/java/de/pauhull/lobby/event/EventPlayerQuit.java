package de.pauhull.lobby.event;

import de.pauhull.lobby.Lobby;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

/**
 * Saves location into database and clears caches after player quit.
 * @author pauhull
 */
public class EventPlayerQuit implements Listener {

    private Lobby lobby;

    /**
     * Listener constructor. Takes an instance of Lobby.
     * @param lobby Lobby instance.
     */
    public EventPlayerQuit(Lobby lobby) {
        this.lobby = lobby;

        Bukkit.getPluginManager().registerEvents(this, lobby.getPlugin());
    }

    /**
     * Handles player quit.
     * @param event Event that gets called.
     */
    @EventHandler
    public void onPlayerQuit(PlayerQuitEvent event) {

        Player player = event.getPlayer();

        for(Player online : Bukkit.getOnlinePlayers()) {
            lobby.getLocale().sendMessage(online, "Info.Quit", false, new String[]{"%PLAYER%", player.getDisplayName()});
        }

        event.setQuitMessage("");

        lobby.getPlayerLocationManager().saveLocation(player.getUniqueId(), player.getLocation());

    }

}
