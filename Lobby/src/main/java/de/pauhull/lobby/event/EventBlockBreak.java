package de.pauhull.lobby.event;

import de.pauhull.lobby.Lobby;
import de.pauhull.lobby.reflection.Reflection;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;

/**
 * Cancels block break.
 * @author pauhull
 */
public class EventBlockBreak implements Listener {

    private Lobby lobby;
    private Material wandMaterial;

    /**
     * Listener constructor. Takes an instance of Lobby.
     * @param lobby Lobby instance.
     */
    public EventBlockBreak(Lobby lobby) {
        this.lobby = lobby;

        this.wandMaterial = Material.getMaterial(lobby.getConfig().getConfig().getString("General.WandMaterial"));

        Bukkit.getPluginManager().registerEvents(this, lobby.getPlugin());
    }

    /**
     * Cancels block break if player hasn't activated <tt>/lobby build</tt>.
     * @param event Event that gets called.
     */
    @EventHandler
    public void onBlockBreak(BlockBreakEvent event) {
        Player player = event.getPlayer();
        Location location = event.getBlock().getLocation();

        if (!lobby.getCanBuild().contains(player)) {
            event.setCancelled(true);

            try {
                Reflection.ParticlePacket packet = new Reflection.ParticlePacket("SMOKE_NORMAL", false, (float) location.getX() + 0.5f,
                        (float) location.getY() + 1f, (float) location.getZ() + 0.5f, 0.25f, 1f, 0.25f, 0, 50, new int[]{0});

                packet.sendPacket(player);
            } catch (Throwable e) {
                e.printStackTrace();
            }

        } else {
            if(player.getItemInHand().getType() == wandMaterial && player.hasPermission("lobby.pos"))
                event.setCancelled(true);
        }
    }

}
