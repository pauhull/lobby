package de.pauhull.lobby.event;

import de.pauhull.lobby.Lobby;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;

public class EventCropTrample implements Listener {

    private Lobby lobby;

    public EventCropTrample(Lobby lobby) {
        this.lobby = lobby;

        Bukkit.getPluginManager().registerEvents(this, lobby.getPlugin());
    }

    @EventHandler
    public void onInteract(PlayerInteractEvent event) {
        Player player = event.getPlayer();
        Action action = event.getAction();

        if(lobby.getCanBuild().contains(player))
            return;

        if(event.hasBlock()) {
            Block block = event.getClickedBlock();

            if(action == Action.PHYSICAL) {
                if(block.getType() == Material.SOIL) {
                    event.setCancelled(true);
                }
            }
        }
    }

}
