package de.pauhull.lobby.event;

import de.pauhull.lobby.Lobby;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;

/**
 * Handles the wand to set positions.
 * @author pauhull
 */
public class EventPlayerInteract implements Listener {

    private Lobby lobby;
    private Material wandMaterial;

    /**
     * Listener constructor. Takes an instance of Lobby.
     * @param lobby Lobby instance.
     */
    public EventPlayerInteract(Lobby lobby) {
        this.lobby = lobby;

        this.wandMaterial = Material.getMaterial(lobby.getConfig().getConfig().getString("General.WandMaterial"));

        Bukkit.getPluginManager().registerEvents(this, lobby.getPlugin());
    }

    /**
     * Handles position setting.
     * @param event Event that gets called.
     */
    @EventHandler
    public void onPlayerInteract(PlayerInteractEvent event) {
        Player player = event.getPlayer();
        Action action = event.getAction();

        if (action != Action.RIGHT_CLICK_BLOCK && action != Action.LEFT_CLICK_BLOCK) {
            return;
        }

        if (player.hasPermission("lobby.pos")) {

            if (player.getItemInHand().getType() == wandMaterial) {
                int i = 1;

                if (action == Action.RIGHT_CLICK_BLOCK) {
                    i = 2;
                }

                if (lobby.getSpawnBoundsCache().containsKey(player.getName())) {
                    Location[] put = lobby.getSpawnBoundsCache().get(player.getName());
                    put[i - 1] = event.getClickedBlock().getLocation();
                    lobby.getSpawnBoundsCache().put(player.getName(), put);
                } else {
                    Location[] put = new Location[2];
                    put[i - 1] = event.getClickedBlock().getLocation();
                    lobby.getSpawnBoundsCache().put(player.getName(), put);
                }

                lobby.getLocale().sendMessage(player, "Info.PositionSet", true, new String[]{"%POS%", String.valueOf(i)});
            }
        }
    }

}
