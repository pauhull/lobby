package de.pauhull.lobby.event;

import de.pauhull.lobby.Lobby;
import de.pauhull.lobby.reflection.Reflection;
import de.pauhull.lobby.util.Util;
import org.bukkit.Bukkit;
import org.bukkit.Effect;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;

/**
 * Kicks player back if he tries to get outside the border.
 * @author pauhull
 */
public class EventPlayerMove implements Listener {

    private Lobby lobby;

    private String particle;
    private double velocity;
    private double yVelocity;

    /**
     * Listener constructor. Takes an instance of Lobby.
     * @param lobby Lobby instance.
     */
    public EventPlayerMove(Lobby lobby) {
        this.lobby = lobby;

        Bukkit.getPluginManager().registerEvents(this, lobby.getPlugin());

        this.particle = lobby.getConfig().getConfig().getString("Border.Particle");
        this.velocity = lobby.getConfig().getConfig().getDouble("Border.Velocity");
        this.yVelocity = lobby.getConfig().getConfig().getDouble("Border.VerticalVelocity");

    }

    /**
     * Handles player move.
     * @param event Event that gets called.
     */
    @EventHandler
    public void onPlayerMove(PlayerMoveEvent event) {

        if(lobby.getSpawnLocation() != null && event.getPlayer().getLocation().getY() < 0)
            event.getPlayer().teleport(lobby.getSpawnLocation());

        if (lobby.getSpawnLocation() == null || lobby.getSpawnBounds()[0] == null || lobby.getSpawnBounds()[1] == null) {
            return;
        }

        Player player = event.getPlayer();

        if (!lobby.getCanBuild().contains(player)) {

            if (player.getLocation().getY() < Math.min(lobby.getSpawnBounds()[0].getY(), lobby.getSpawnBounds()[1].getY())
                    || !Util.inBounds(player.getLocation(), lobby.getSpawnBounds()[0], lobby.getSpawnBounds()[1], 1D)) {
                player.teleport(lobby.getSpawnLocation());
                player.playSound(player.getLocation(), Sound.WITHER_SHOOT, 1, 1);
                return;
            }

            if (!Util.inBounds(player.getLocation(), lobby.getSpawnBounds()[0], lobby.getSpawnBounds()[1], 0.1D)) {

                if (lobby.getSpawnLocation() != null) {
                    player.setVelocity(Util.calculateVector(player.getLocation(), lobby.getSpawnLocation()).multiply(velocity).setY(yVelocity));
                } else {
                    player.setVelocity(player.getLocation().getDirection().multiply(-1.5D).setY(yVelocity));
                }

                player.playEffect(player.getLocation(), Effect.ZOMBIE_DESTROY_DOOR, 1);

                try {
                    Reflection.ParticlePacket packet = new Reflection.ParticlePacket(particle, false, (float) player.getLocation().getX(),
                            (float) player.getLocation().getY(), (float) player.getLocation().getZ(), 1.5F, 1.5F, 1.5F, 0, 50, new int[]{0});

                    packet.sendPacket(player);
                } catch (Throwable e) {
                    e.printStackTrace();
                }

            }
        }
    }

}
