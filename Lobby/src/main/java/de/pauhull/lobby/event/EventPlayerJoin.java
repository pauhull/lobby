package de.pauhull.lobby.event;

import de.pauhull.lobby.Lobby;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.potion.PotionEffect;

/**
 * Resets player on join.
 * @author pauhull
 */
public class EventPlayerJoin implements Listener {

    private Lobby lobby;

    /**
     * Listener constructor. Takes an instance of Lobby.
     * @param lobby Lobby instance.
     */
    public EventPlayerJoin(Lobby lobby) {
        this.lobby = lobby;

        Bukkit.getPluginManager().registerEvents(this, lobby.getPlugin());
    }

    /**
     * Resets player stats.
     * @param event Event that gets called.
     */
    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent event) {
        Player player = event.getPlayer();

        for(Player online : Bukkit.getOnlinePlayers()) {
            lobby.getLocale().sendMessage(online, "Info.Join", false, new String[]{"%PLAYER%", player.getDisplayName()});
        }

        event.setJoinMessage("");

        player.setGameMode(GameMode.valueOf(lobby.getConfig().getConfig().getString("General.DefaultGameMode")));

        player.getInventory().clear();
        player.getInventory().setArmorContents(null);
        player.setFoodLevel(20);
        player.setFireTicks(0);
        player.setLevel(0);
        player.setExp(0F);

        double health = lobby.getConfig().getConfig().getDouble("General.LobbyHealth");

        player.setMaxHealth(health);
        player.setHealth(health);

        for (PotionEffect effect : player.getActivePotionEffects()) {
            player.removePotionEffect(effect.getType());
        }
    }

}
